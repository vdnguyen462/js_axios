const BASE_URL = "https://63bfa3c2a177ed68abb4c90a.mockapi.io"

function renderFoodList (foods){
    var contentHTML = "";
    foods.forEach(function(item){
        var contentTr = `<tr>
                        <td>${item.maMon}</td>
                        <td>${item.tenMon}</td>
                        <td>${item.giaMon}</td>
                        <td>${item.loaiMon? "<span class='text-primary'>Mặn</span>" : 
                        "<span class='text-danger'>Chay</span>"}
                        </td>
                        <td>${ convertString(30, item.hinhAnh)}</td>
                        <td><button onclick="xoaMon('${item.maMon}')" class="btn-danger">Xoá</button>
                        <button  onclick="suaMon('${item.maMon}')" class="btn-primary">Sửa</button></td>
                        </tr>`;
        contentHTML+=contentTr;
    });
    document.getElementById("tbodyFood").innerHTML = contentHTML;

}

var foodList = [];
function fetchFoodList(){
    batLoading();
    axios({
        url: `${BASE_URL}/food`,
        method: "GET",
    })
        .then(function(res){
            tatLoading();
            foodList = res.data;
            renderFoodList(foodList);
        })
        .catch(function (err){
            tatLoading();
            console.log("🚀 ~ file: main.js:39 ~ fetchFoodList ~ err", err);   
        })
}

fetchFoodList();


function xoaMon(id){
    batLoading();
    axios({
        url: `${BASE_URL}/food/${id}`,
        method: "DELETE",
    }).then(function(res){
        // gọi lại API sau khi user xoá thành công
        tatLoading();
        fetchFoodList();
    })
    .catch(function(err){
        tatLoading();
        console.log("🚀 ~ file: main.js:50 ~ xoaMon ~ err", err);
    })
}


function themMonAn(){
    var monAn = layThongTinTuForm();
    axios ({
        url: `${BASE_URL}/food`,
        method: "POST",
        data: monAn,
    }).then(function(res){
        renderFoodList();
    }).catch(function(err){
        console.log("🚀 ~ file: main.js:82 ~ themMonAn ~ err", err);
    });
}


function suaMon(id){
    batLoading();
    axios({
        url: `${BASE_URL}/food/${id}`,
        method: "GET",
    }).then(function(res){
        tatLoading();
        document.getElementById("txtMaMon").readOnly = true;
        showThongTinLenForm(res.data);
    }).catch(function(err){
        tatLoading();
        console.log("🚀 ~ file: main.js:94 ~ suaMonAn ~ err", err);
    })
}


function capNhatMonAn(){
    batLoading();
    var monAn = layThongTinTuForm();
    axios({
        url: `${BASE_URL}/food/${monAn.maMon}`,
        method: "PUT",
        data: monAn,
    }).then(function(res){
        tatLoading();
        resetThongTin();
        renderFoodList();
    }).catch(function(err){
        tatLoading();
        console.log("🚀 ~ file: main.js:111 ~ capNhatMonAn ~ err", err)
    });
}


