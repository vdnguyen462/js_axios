function batLoading(){
    document.getElementById("spinner").style.display = "flex";
}

function tatLoading(){
    document.getElementById("spinner").style.display = "none";
}

function layThongTinTuForm(){
    const _maMon = document.getElementById("txtMaMon").value;
    const _tenMon = document.getElementById("txtTenMon").value;
    const _giaMon = document.getElementById("txtGiaMon").value;
    const _loaiMon = document.getElementById("txtLoaiMon").value;
    const _hinhMon = document.getElementById("txtHinhAnh").value;

    var monAn = {
        maMon: _maMon, 
        tenMon: _tenMon, 
        giaMon: _giaMon, 
        hinhAnh: _hinhMon,
        loaiMon: _loaiMon, 
    };
    return monAn;
}

function convertString(maxLength, value){
    if(value.length > maxLength){
        return value.slice(0, 30) + "...";
    }
    else{
        return value;
    }
}

function showThongTinLenForm(item){
    document.getElementById("txtMaMon").value = item.maMon;
    document.getElementById("txtTenMon").value = item.tenMon;
    document.getElementById("txtGiaMon").value = item.giaMon;
    document.getElementById("txtLoaiMon"). value = item.loaiMon ? "Mặn":"Chay";
    document.getElementById("txtHinhAnh").value = item.hinhAnh;
}

function resetThongTin(){
    document.getElementById("txtMaMon").value = "";
    document.getElementById("txtTenMon").value = "";
    document.getElementById("txtGiaMon").value = "";
    document.getElementById("txtLoaiMon").value = "";
    document.getElementById("txtHinhAnh").value = "";
}